@extends('layouts.app')
@section('title')
    Profile
@endsection
@section('content')
    @include('layouts.alert.alert')
    <div class="row">

        <div class="col-md-6 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">User Detail</h4>

                    <form class="forms-sample" method="POST" action="{{route('profile.update')}}" enctype="multipart/form-data">
                        @csrf
                        @method('PATCH')
                        <div class="form-group">
                            <label for="exampleInputUsername1">{{ __('Name') }}</label>
                            <input type="text" class="form-control" name="name" id="name" value="{{ old('name', auth()->user()->name) }}" placeholder="{{ __('Name') }}">
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail1">{{ __('Email address') }}</label>
                            <input type="email" class="form-control" id="email" name="email" value="{{ old('email', auth()->user()->email) }}" placeholder="{{ __('Email address') }}">
                        </div>
                        <div class="form-group">
                            <label for="exampleInputPhone">{{ __('Phone') }}</label>
                            <input type="text" class="form-control" name="phone" id="phone" value="{{ old('phone', auth()->user()->phone) }}" placeholder="{{ __('Phone Number') }}" >
                        </div>
                        <div class="form-group">

                            <label for="exampleInputImage">{{__('Image') }}</label>
                            <input type="file" class="form-control" name="user_image" id="image">
                            <img id="user_image" src="" alt="user image" width="100px;" style="display: none"/>
                            <img id="old_user_image" src="{{asset('admin/images/user/'.auth()->user()->image)}}" alt="user image" width="100px;" />
                        </div>
                        <div class='dropzone'></div>
                        <button type="submit" class="btn btn-primary btn-rounded mr-2">{{ __('Update') }}</button>

                    </form>
                </div>
            </div>
        </div>
        <div class="col-md-6 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">{{ __('PASSWORD') }}</h4>

                    <form class="forms-sample" autocomplete="off" method="post" action="{{route('updateOldPassword')}}">
                        @csrf
                        <div class="form-group row">
                            <label for="exampleInputPassword2" class="col-sm-4 col-form-label">{{ __('Current Password') }}</label>
                            <div class="col-sm-8">
                                <input type="password" name="old_password" class="form-control" id="old_password"
                                       placeholder="{{ __('Current Password') }}"  data-ajax="{{ route('checkOldPassword') }}">
                                <div class="form-group" id="message"></div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="exampleInputConfirmPassword2" class="col-sm-4 col-form-label">{{ __('New Password') }}</label>
                            <div class="col-sm-8">
                                <input type="password" name="new_password" class="form-control" id="exampleInputConfirmPassword2" placeholder="{{ __('New Password') }}">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="exampleInputConfirmPassword2" class="col-sm-4 col-form-label">{{ __('Confirm New Password') }}</label>
                            <div class="col-sm-8">
                                <input type="password" name="password_confirmation" class="form-control"  placeholder="{{ __('Confirm New Password') }}">
                            </div>
                        </div>
                        <button type="submit" class="btn btn-primary btn-rounded mr-2">{{ __('Change Passowrd') }}</button>

                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('script')

  <script>

      function readURL(input) {


          if (input.files && input.files[0]) {

              var reader = new FileReader();
              reader.onload = function(e) {
                  $('#user_image').show();
                  $('#old_user_image').hide();
                    $('#user_image').attr('src', e.target.result).slideDown();;
              };

              reader.readAsDataURL(input.files[0]);
          }
      }

      $("#image").change(function() {
          readURL(this);
      });
  </script>
@endsection

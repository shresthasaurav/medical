<nav class="sidebar sidebar-offcanvas" id="sidebar">
    <ul class="nav">
        <li class="nav-item">
            <a class="nav-link" href="{{url('dashboard')}}">
                <i class="mdi mdi-home menu-icon"></i>
                <span class="menu-title">{{ __('Dashboard') }}</span>
            </a>
        </li>

        <li class="nav-item">
            <a class="nav-link" href="{{route('profile.edit')}}">
                <i class="mdi mdi-account menu-icon"></i>
                <span class="menu-title">{{ __('Profile') }}</span>
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="{{route('information.edit')}}">
                <i class="mdi mdi-home-variant menu-icon"></i>
                <span class="menu-title">{{ __('Office Profile') }}</span>
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="{{route('patients.index')}}">
                <i class="mdi mdi-emoticon menu-icon"></i>
                <span class="menu-title">{{ __('Patients') }}</span>
            </a>
        </li>

    </ul>
</nav>

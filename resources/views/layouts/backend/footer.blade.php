<footer class="footer">
    <div class="d-sm-flex justify-content-center justify-content-sm-between">
        <span class="text-muted d-block text-center text-sm-left d-sm-inline-block">&copy; {{ now()->year }} <a href="https://rarait.com" class="font-weight-bold ml-1" target="_blank">Rara IT</a></span>
    </div>
</footer>

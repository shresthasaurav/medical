@if ($message = Session::has('flash_success_message'))

    <div class="alert alert-success alert-dismissible fade show">
        <strong>Success!</strong> {{ Session('flash_success_message') }}
        <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
@endif


@if ($message = Session::has('flash_error_message'))

    <div class="alert alert-danger alert-dismissible">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        <h4><i class="icon fa fa-ban"></i> Alert!</h4>
        {{ Session('flash_error_message') }}
    </div>

@endif

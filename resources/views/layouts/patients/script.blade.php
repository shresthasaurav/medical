<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>

<script>

    function readURL(input) {


        if (input.files && input.files[0]) {

            var reader = new FileReader();
            reader.onload = function(e) {
                $('#patient_image').show();
                $('#patient_image1').hide();
                $('#patient_image').attr('src', e.target.result).slideDown();;
            };

            reader.readAsDataURL(input.files[0]);
        }
    }

    $("#image").change(function() {
        readURL(this);
    });
</script>
@yield('script')

<div class="wrap">
    <div class="logo">
        <img src="{{asset('admin/images/logo/'.$site->logo)}}" style="width: 190px;">
    </div><!-- close logo -->
    <div class="user-image mobile-image">
        Gov-Reg-No {{ $site->gov_regno ?? null }}
        <img src="https://rmiofmaryland.com/wp-content/uploads/2015/10/MHC_5150_PP.jpg" style="width: 150px;">
    </div>
    <div class="content">
        <small>{{ $site->title ?? null }}</small>
        <h3 >{{ $site->company_name ?? null }}</h3>

        <small>{{ $site->address ?? null }},{{ $site->city ?? null }}, Tel:{{ $site->phone ?? null }},{{ $site->landline ?? null }},{{ $site->landline1 ?? null }}</small>

        <div class="address">
            <small>Email : {{ $site->email ?? null }}</small><br>
            <small>Website :{{ $site->website ?? null }}</small>
        </div><!-- close content -->
        <p style="font-weight: 400;">(Affiliated to {{$site->affiliated ?? null }})</p>
        <p class="report">{{ $site->title ?? null }}</p>
    </div><!-- close content -->

    <div class="user-image mobile-user-image">
        Gov-Reg-No {{ $site->gov_regno ?? null }}

        @if(!empty($patient) && $patient->image)
            <img  src="{{asset('admin/images/patients/'.$patient->image)}}" id="patient_image1" alt="patient_image" width="100px;" />
        @endif
        <img id="patient_image" src="" alt="patient_image" width="100px;" style="display: none"/>
    </div>
    <div class="clear"></div>

</div>

@extends('layouts.patients.app')
@section('title')
    Edit Record
@endsection
@section('content')
    <form method="post" action="{{route('patients.update',$patient)}}" enctype="multipart/form-data">
        @csrf
        @method('patch')

        @include('patients.form')

    </form>
@endsection

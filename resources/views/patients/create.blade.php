@extends('layouts.patients.app')
@section('title')
    Create Record
@endsection
@section('content')
    <form method="post" action="{{route('patients.store')}}" enctype="multipart/form-data">
        @csrf
        @include('patients.form')
    </form>
@endsection


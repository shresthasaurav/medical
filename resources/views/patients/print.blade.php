<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="{{asset('admin/patients/print/css/bootstrap.min.css')}}" rel="stylesheet">
    <link rel="stylesheet" href="{{asset('admin/patients/print/css/style.css')}}">
    <title>Document</title>
</head>
<body>

<div class="container text center">
    <div class="medical-text">
        <div class="wrap">
            <div class="logo">
                <img src="https://www.onlinelogomaker.com/blog/wp-content/uploads/2017/07/medical-logo.jpg">
            </div><!-- close logo -->
            <div class="user-image mobile-image">
                Gov-Reg-No {{$site->gov_regno ?? null}}
                <img src="https://rmiofmaryland.com/wp-content/uploads/2015/10/MHC_5150_PP.jpg" style="width: 150px;">
            </div>
            <div class="content">
                <small>{{ $site->title ?? null }}</small>
                <h3 >{{ $site->company_name ?? null }}</h3>

                <small>{{ $site->address ?? null }},{{ $site->city ?? null }},Tel:{{ $site->phone ?? null }},{{ $site->landline ?? null }},{{ $site->landline1 ?? null }}</small>
                <div class="address">
                    <small>Email : {{ $site->email ?? null }}</small><br>
                    <small>Website : {{ $site->website ?? null }}</small>
                </div><!-- close content -->
                <p style="font-weight: 400; margin-bottom: 5px;">(Affiliated to {{$site->affiliated ?? null }})</p>
                <p class="report">{{$site->title ?? null}}</p>
            </div><!-- close content -->

            <div class="user-image mobile-user-image">
                Gov-Reg-No 60496-065/066
                <img src="{{asset('admin/images/patients/'.$patient->image)}}" style="width: 150px;">
            </div>
            <div class="clear"></div>

        </div><!-- close wrap -->
        <div class="result-detail">
            ​   <div class="table table-head top-head">

                <table class="table-wrap">
                    <p  class="text-fix" style="vertical-align: middle; text-align: center; border: 1px solid #ddd; margin-bottom: 0; position: relative; top:21px;  width:100px; font-size: 10px;  padding: 2px; margin: auto; font-weight: bold;" >{{ $patient->status ?? null }}</p>
                    <thead>
                    <tr>
                        <th>EMC Code 1153</th>

                        <th></th>
                        <th>Medical Examination Date: {{ isset($patient) ? date("Y-m-d", strtotime($patient->medical_examination_date)):date("Y-m-d") }}</th>
                    </tr>
                    </thead>

                    <tbody>
                    <tr>
                        <td>Name:{{$patient->name ?? null}}</td>
                        <td>Marital Status :{{$patient->marital_status ?? null }} </td>
                        <td>Apply Country: {{$patient->apply_county  ?? null }}</td>
                    </tr>
                    <tr>
                        <td>Age:{{$patient->age  ?? null }}</td>
                        <td>Nationlity : {{$patient->nationality  ?? null }}</td>
                        <td>PP:Issue date & Place : {{$patient->pp_issue_date  ?? null }} AT {{$patient->pp_issue_place  ?? null }}</td>
                    </tr>
                    <tr>
                        <td>Sex:{{$patient->sex  ?? null }}</td>
                        <td>PP NO: {{$patient->pp_no  ?? null }}</td>
                        <td>Security Agencies : {{$patient->recruiting_agency  ?? null }}</td>
                    </tr>
                    </tbody>
                </table>
            </div><!-- close table -->
            <div class="table table-head">

                <table class="table-wrap">
                    <p style="vertical-align: middle; margin-bottom: 0; font-size: 10px;" >Post history of setous lines and major surgery . {!!  $patient->history_illness ?? null !!}</p>

                    <tbody>
                    <tr>
                        <td> {{ __('Height:') }} : {{$patient->height  ?? null }} </td>
                        <td>{{ __('Pulse') }} : {{$patient->pulse  ?? null }} </td>
                        <td>{{ __('Jaundice') }} : {{$patient->jaundice  ?? null }} </td>
                        <td> {{ __('Ascites') }} : {{$patient->ascites  ?? null }} </td>
                        <td> {{ __('Lymph Node') }} : {{$patient->lymph_node  ?? null }} </td>
                    </tr>
                    <tr>
                        <td>{{ __('Weight') }} : {{$patient->weight  ?? null }}</td>
                        <td>{{ __('BP') }} : {{$patient->bp  ?? null }}
                        <td>{{ __('Oedema') }} : {{$patient->oedema  ?? null }}</td>
                        <td>{{ __('Allergy') }} : {{$patient->allergy  ?? null }} </td>
                        <td>{{ __('Fungal infection') }} : {{$patient->fungal_infection  ?? null }}</td>
                    </tr>
                    <tr>
                        <td>{{ __('Temperature')}} : {{$patient->temp  ?? null }} </td>
                        <td>{{ __('Clubbing')}} : {{$patient->clubbing  ?? null }}</td>
                        <td>{{ __('Paller')}} : {{$patient->paller  ?? null }}</td>
                        <td>{{ __('Cyanosis')}} : {{$patient->cyanosis  ?? null }}</td>
                        <td>{{ __('Epilepsy/Depression')}} : {{$patient->depression  ?? null }}</td>
                    </tr>

                    </tbody>
                </table>
            </div><!-- close table -->
            <div class="left-side">
                <div class=" table-side">
                    <p style="vertical-align: middle; text-align: center; border: 1px solid #ddd;background: #000; font-size: 10px; width: 200px; color: #fff; padding: 5px; font-weight: bold; margin-bottom: 0;">SYSTRMIC EXAMINATION </p>
                    <table class="table">

                        <thead>
                        <th colspan="2">TYpe of Examination</th>

                        <th style="text-align: left;">Result</th>
                        </thead>
                        <tbody>
                        <tr valign="center" style="border: 1px solid #ddd;">
                            <td rowspan="3" style="vertical-align: middle; border-right: 1px solid #ddd;">
                                Eye Vision</td>
                            <td>R.Eye</td>
                            <td>{{$patient->right_eye  ?? null }}</td>
                        </tr>
                        <tr>

                            <td>L.Eye</td>
                            <td>{{$patient->left_eye  ?? null }}</td>

                        </tr>
                        <tr>

                            <td>Color Vission</td>
                            <td>{{ $patient->color_vision  ?? null }}</td>

                        </tr>
                        <tr valign="center" >
                            <td rowspan="2" style="vertical-align: middle;">Ear</td>
                            <td>R.Ear</td>
                            <td>{{ $patient->right_ear  ?? null }}</td>

                        </tr>
                        <tr>
                            <td>L.Ear</td>
                            <td>{{ $patient->left_ear  ?? null }}</td>

                        </tr>
                        <tr>
                            <td colspan="2">Cordiovascular</td>
                            <td>{{ $patient->cardiovascular ?? null }}</td>
                        </tr>
                        <tr>
                            <td colspan="2">Pulrnonary</td>
                            <td>{{  $patient->pulmonary ?? null }}</td>
                        </tr>
                        <tr>
                            <td colspan="2">Gastroenterology</td>
                            <td>{{  $patient->gastroenterology ?? null }}</td>
                        </tr>
                        <tr>
                            <td colspan="2">Neorology</td>
                            <td>{{  $patient->neurology ?? null }}</td>
                        </tr>
                        <tr>
                            <td colspan="2">Museulosheletal</td>
                            <td>{{ $patient->musculoskeletal ?? null }} </td>
                        </tr>
                        <tr>
                            <td colspan="2">Genitounnary</td>
                            <td>{{ $patient->genitourinary ?? null }}</td>
                        </tr>
                        <tr>
                            <td colspan="2">Ore-DEntail</td>
                            <td>{{ $patient->oro_dental ?? null }} </td>
                        </tr>
                        <tr>
                            <td colspan="2">Extremilies/Deformities</td>
                            <td>{{ $patient->extremities ?? null }}</td>
                        </tr>
                        <tr>
                            <td colspan="2">Varicose Veins</td>
                            <td>{{ $patient->varicose_veins ?? null }}</td>
                        </tr>
                        <tr>
                            <td colspan="2">Hernia</td>
                            <td>{{ $patient->hernia ?? null }}</td>
                        </tr>
                        <tr>
                            <td colspan="2">Hydrocele</td>
                            <td>{{$patient->hydrocele ?? null }}</td>
                        </tr>
                        <tr>
                            <td colspan="2">Radiological(Chest X-ray)</td>
                            <td>{{  $patient->radiological ?? null }}</td>
                        </tr>
                        <tr>
                            <td colspan="2">ECG</td>
                            <td>{{ $patient->ecg ?? null }}</td>
                        </tr>
                        <tr>
                            <td colspan="2">Heart</td>
                            <td>{{ $patient->heart ?? null }}</td>
                        </tr>
                        <tr>
                            <td colspan="2">Lungs</td>
                            <td>{{ $patient->lungs ?? null }}</td>
                        </tr>
                        <tr>
                            <td colspan="2">Abidamon</td>
                            <td>{{ $patient->abdomen ?? null }}</td>
                        </tr>
                        <tr>
                            <td colspan="2">Clinical Impression</td>
                            <td>{{ $patient->clinical_impression ?? null }}</td>
                        </tr>

                        </tbody>

                    </table>
                </div><!-- close table-side -->

                <p class="mention">{{$patient->name}} is {{$patient->status}} for the Mentioned job.</p><br>
                <div class="text-border1">
                    <p class="text-contain">This Report is vaild for 2 month the date of medical Examination DR.Nabin Kumar chaudhary</p>
                </div>
            </div><!-- close left-side -->
            <div class="right-side">
                <div class="table-resposive">
                    <p style="vertical-align: middle; text-align: center; border: 1px solid #ddd; background: #000; width:200px; font-size: 10px; color:#fff; padding: 2px; font-weight: bold; margin-bottom: 0; " >LABORATORY EXAMINATION</p>
                    <table class="table">

                        <p style="vertical-align: middle; text-align: center; border: 1px solid #ddd; font-weight: bold; margin-bottom: 0;" >Serology</p>

                        <thead>
                        <th>Examination</th>
                        <th style="text-align: left;">Result</th>
                        </thead>
                        <tbody>
                        <tr>
                            <td>Anti-HIV(1&2)</td>
                            <td>{{ $patient->anti_hiv ?? null }}</td>
                        </tr>
                        <tr>
                            <td>HBS-AQ </td>
                            <td>{{ $patient->hbs_ag ?? null }} </td>
                        </tr>
                        <tr>
                            <td>Anti-HCV  </td>
                            <td>{{ $patient->anti_hcv ?? null }}</td>
                        </tr>
                        <tr>
                            <td>VDRL</td>
                            <td>{{  $patient->vdrl ?? null }} </td>
                        </tr>
                        <tr>
                            <td>TPHA</td>
                            <td>{{ $patient->tpha ?? null }} </td>
                        </tr>
                        <tr>
                            <td>Blood Group(ABO/Rh)</td>
                            <td>{{ $patient->blood_group ?? null }}</td>
                        </tr>
                        <tr>
                            <td colspan="2" style="text-align: center;"><strong>Urine</strong></td>
                        </tr>
                        <tr>
                            <td>RBC</td>
                            <td> {{$patient->rbc ?? null }}</td>
                        </tr>
                        <tr>
                            <td>Pus Cells</td>
                            <td>{{  $patient->pus_ceils ?? null }}</td>
                        </tr>
                        <tr>
                            <td>Bacteria</td>
                            <td> {{ $patient->bacteria ?? null }}</td>
                        </tr>
                        <tr>
                            <td>Opiates</td>
                            <td>{{  $patient->opiates ?? null }} </td>
                        </tr>
                        <tr>
                            <td>Epithelial Cells</td>
                            <td>{{  $patient->epithelial_cells ?? null }} </td>
                        </tr>
                        <tr>
                            <td>Pregnancy Test (If Female)</td>
                            <td>{{  $patient->pregnancy_test ?? null }}</td>
                        </tr>
                        </tbody>
                    </table>
                </div><!-- table-resposive-->
                <div class="table-resposive">
                    <table class="table">

                        <p style="vertical-align: middle; border: 1px solid #ddd; font-weight: bold; font-size: 10px; margin-bottom: 0;  padding: 2px; background: #000; color: #fff; width: 130px; margin-top: -21px" >HEMATOLOGY</p>

                        <thead>
                        <th>Blood Examination</th>
                        <th style="text-align: left;">Result</th>
                        <th style="text-align: left;">Reference range</th>
                        </thead>
                        <tbody>
                        <tr>
                            <td>Total/WBC Count</td>
                            <td>{{  $patient->total_wbc_count ?? null }}</td>
                            <td>4000-11000</td>
                        </tr>


                        </tbody>
                    </table>
                </div><!-- table-resposive-->
                <div class="table-resposive">
                    <table class="table">

                        <p style="vertical-align: middle; border: 1px solid #ddd; font-weight: bold; margin-bottom: 0; font-size: 10px;  padding: 2px; margin-top: -21px;" >Differential Count</p>
                        <tbody>
                        <tr>
                            <td>Neutrt Phils</td>
                            <td>{{ $patient->neutrophils ?? null }}</td>
                            <td>45.74%</td>
                        </tr>
                        <tr>
                            <td>Lymprocytes</td>
                            <td>{{ $patient->lymphocytes ?? null }}</td>
                            <td>25-40%</td>
                        </tr>
                        <tr>
                            <td>Eosinu Phils</td>
                            <td>{{  $patient->eosinophils ?? null }}</td>
                            <td>1-6%</td>
                        </tr>
                        <tr>
                            <td>Monocytes</td>
                            <td>{{ $patient->monocytes ?? null }}</td>
                            <td>0-8%</td>
                        </tr>
                        <tr>
                            <td>Basophils</td>
                            <td>{{ $patient->basophils ?? null }}</td>
                            <td>0-3%</td>
                        </tr>
                        <tr>
                            <td>ESR</td>
                            <td>{{ $patient->esr ?? null }}</td>
                            <td>M<10,F<20</td>
                        </tr>
                        <tr>
                            <td>Hemoglobin</td>
                            <td>{{ $patient->hemoglobin ?? null }}</td>
                            <td>M 12-17 gm% <br> F 11-14 gm%</td>
                        </tr>
                        <tr>
                            <td>Malaria Parasite</td>
                            <td>{{ $patient->malaria_parasite ?? null }}</td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>Micro Filaria</td>
                            <td>{{ $patient->micro_filaria ?? null }}</td>
                            <td></td>
                        </tr>
                        </tbody>
                    </table>
                </div><!-- table-resposive-->
                <div class="table-resposive">
                    <table class="table">

                        <p style="vertical-align: middle; border: 1px solid #ddd; font-weight: bold; margin-bottom: 0;  padding: 2px; font-size: 10px; background: #000; color: #fff; width: 130px; margin-top: -21px;" >BIOCHEMISTRY</p>

                        <thead>
                        <th>Randan Blood Sugar</th>
                        <th style="text-align: left;">{{ $patient->random_blood_sugar ?? null }}</th>
                        <th style="text-align: left;">80-30mg%</th>
                        </thead>
                        <tbody>
                        <tr>
                            <td>Urea</td>
                            <td>{{ $patient->urea ?? null }}</td>
                            <td>20-25mg%</td>
                        </tr>
                        <tr>
                            <td>Crealnine</td>
                            <td> {{ $patient->creatinine ?? null }}</td>
                            <td>04-14mg%</td>
                        </tr>
                        <tr>
                            <td> Total Direct</td>
                            <td>{{  $patient->bilirubin ?? null }} </td>
                            <td>0.4-1mg%</td>
                        </tr>
                        <tr>
                            <td>SGPT</td>
                            <td>{{ $patient->sgpt ?? null }} </td>
                            <td>5-37 UL</td>
                        </tr>
                        <tr>
                            <td>SGOT</td>
                            <td>{{  $patient->sgot ?? null }}</td>
                            <td>5-37 UL</td>
                        </tr>
                        <tr>
                            <td colspan="3">Other</td>

                        </tr>
                        <tr>
                            <td>Cannabis</td>
                            <td colspan="2">{{$patient->cannabis ?? null }}</td>
                        </tr>
                        <tr>
                            <td>Mantoex</td>
                            <td colspan="2">{{ $patient->mantoux_test ?? null }}</td>
                        </tr>
                        </tbody>
                    </table>
                </div><!-- table-resposive-->
            </div><!-- right-side -->

            <div class="clear"></div>


        </div><!--result-detail -->
    </div><!-- medical-text -->
    <div class="footer">
        <div class="first-signature">
            <span class="border"></span>
            <p>Technologist Signature</p>
        </div>
        <div class="first-signature">
            <span class="border"></span>
            <p>Authorized signature </p>
        </div>
        <div class="first-signature last-signature">
            <span class="border"></span>
            <p>Doctor Signature</p>
        </div>
    </div>

</div><!-- close container -->

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
<script src="{{asset('admin/patients/print/js/bootstrap.min.js')}}"></script>
<script>
    window.onload = function () {
        window.print();
    }
</script>
</body>
</html>
















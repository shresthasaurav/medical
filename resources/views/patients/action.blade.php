<td>
    <div class="d-flex">
    <a href="{{ route('patients.show', $patient) }}" title="View Record">
        <i class="mdi mdi-eye" style="width: 1px"></i>
    </a>
    <a href="{{ route('patients.edit', $patient) }}" title="Edit Record">
        <i class="mdi mdi-grease-pencil" style="color: orange"></i>
    </a>
    <a href="#!"
       onclick="confirm('{{ __("Are you sure you want to delete this item?") }}') ?  document.getElementById('delete-patient').submit() : ''">
        <i class="mdi mdi-delete" style="color: red" title="Delete Record">

        </i>
    </a>

    <form action="{{ route('patients.destroy', $patient) }}" method="post" id="delete-patient">
        @csrf
        @method('DELETE')
    </form>
    <a href="{{route('patients.print',$patient)}}" title="Print Record">
        <i class="mdi mdi-cloud-print-outline" style="color: saddlebrown"></i>
    </a>

    <a href="{{route('patientExport',$patient)}}" title="Export Record"><i class="mdi mdi-export" style="color: green"></i></a>
    </div>
</td>


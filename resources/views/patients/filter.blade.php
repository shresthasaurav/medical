<div class="col-12 grid-margin stretch-card">
    <div class="card">
        <div class="card-body">
            <h4 class="card-title">Filter Patient</h4>
            <form class="form-inline">
                <label class="sr-only" >Age</label>
                <input type="number" class="form-control mb-2 mr-sm-2" name="min_age"  value="{{ $activefilters['min_age'] ?? '' }}" placeholder="Min Age">
                <input type="number" class="form-control mb-2 mr-sm-2" name="max_age"  value="{{ $activefilters['max_age'] ?? '' }}" placeholder="Max Age">

                <label class="sr-only" for="medical_examination_date" >Medical Examination Date</label>
                <input type="date" class="form-control mb-2 mr-sm-2 date" value="{{ $activefilters['from'] ?? '' }}" name="from" placeholder="From">
                <input type="date" class="form-control mb-2 mr-sm-2 date" value="{{ $activefilters['to'] ?? '' }}" name="to"  placeholder="To">

                <label class="sr-only" for="gender">Gender</label>
                <div class="input-group mb-2 mr-sm-2">
                    <select class="form-control " name="gender"  id="gender" >
                        <option value=" ">Select Gender</option>
                        <option value="Male" {{ ($activefilters['gender'] ?? '') == "Male" ? 'selected' : '' }}>Male</option>
                        <option value="Female" {{ ($activefilters['gender'] ?? '') == "Female" ? 'selected' : '' }}>Female</option>
                    </select>
                </div>
                <div class="input-group mb-2 mr-sm-2">
                    <select class="form-control " name="status"  id="status" >
                        <option value=" ">Select Status</option>
                        <option value="FIT" {{ ($activefilters['status'] ?? '') == "FIT" ? 'selected' : '' }}>FIT</option>
                        <option value="UNFIT" {{ ($activefilters['status'] ?? '') == "UNFIT" ? 'selected' : '' }}>UNFIT</option>
                    </select>

                </div>

                <button type="submit" class="btn btn-primary mb-2">Filter</button>
            </form>
        </div>
    </div>
</div>


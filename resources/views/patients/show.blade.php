@extends('layouts.patients.app')
@section('title')
    View Record
@endsection
@section('content')
    <div class="table table-head">

    <table class="table-wrap">
        <p style="vertical-align: middle; text-align: center; border: 1px solid #ddd;  width:100px;  padding: 5px; margin: auto; font-weight: bold;" >{{ $patient->status ?? null }}</p>
        <thead>
        <tr>
            <th>EMC Code 1153</th>

            <th></th>
            <th>Medical Examination Date: {{ isset($patient) ? date("Y-m-d", strtotime($patient->medical_examination_date)):date("Y-m-d") }}</th>
        </tr>
        </thead>

        <tbody>
        <tr>
            <td>  <label>SEX:{{ __('Name') }} : {{$patient->name ?? null}}</label> </td>
            <td><label>SEX:{{ __('Marital Status') }} : {{$patient->marital_status ?? null }}</label></td>
            <td><label>SEX:{{ __('Apply Country') }} : {{$patient->apply_county  ?? null }}</label> </td>
        </tr>
        <tr>
            <td><label>SEX:{{ __('Age') }} : {{$patient->age  ?? null }}</label> </td>
            <td><label>SEX:{{ __('Nationlity') }} : {{$patient->nationality  ?? null }}</label> </td>
            <td><label>SEX:{{ __('PP:Issue date & place') }} : {{$patient->pp_issue_date  ?? null }} at {{$patient->pp_issue_place  ?? null }}</label></td>
        </tr>
        <tr>
            <td><label>SEX:{{ __('SEX') }} : {{$patient->sex  ?? null }}</label></td>
            <td><label>SEX:{{ __('PP NO:') }} : {{$patient->pp_no  ?? null }}</label></td>
            <td><label>SEX:{{ __('Recruiting Agency:') }} : {{$patient->recruiting_agency  ?? null }}</label></td>
        </tr>
        </tbody>
    </table>
</div>
<div class="table table-head">

    <table class="table-wrap">
        <p style="vertical-align: middle; border: 1px solid #ddd; padding-left: 10px;" >Post history of setous lines and major surgery : {!!  $patient->history_illness ?? null !!}</p>

        <tbody>
        <tr>
            <td> <label>{{ __('Height:') }} : {{$patient->height  ?? null }}</label> </td>
            <td><label>{{ __('Pulse') }} : {{$patient->pulse  ?? null }}</label> </td>
            <td><label>{{ __('Jaundice') }} : {{$patient->jaundice  ?? null }}</label> </td>
            <td><label> {{ __('Ascites') }} : {{$patient->ascites  ?? null }}</label> </td>
            <td><label> {{ __('Lymph Node') }} : {{$patient->lymph_node  ?? null }}</label> </td>
        </tr>
        <tr>
            <td><label>{{ __('Weight') }} : {{$patient->weight  ?? null }}</label></td>
            <td><label>{{ __('BP') }} : {{$patient->bp  ?? null }}</label></td>
            <td><label>{{ __('Oedema') }} : {{$patient->oedema  ?? null }}</label></td>
            <td><label>{{ __('Allergy') }} : {{$patient->allergy  ?? null }}</label> </td>
            <td><label>{{ __('Fungal infection') }} : {{$patient->fungal_infection  ?? null }}</label></td>
        </tr>
        <tr>
            <td><label>{{ __('Temperature')}} : {{$patient->temp  ?? null }} </label></td>
            <td><label>{{ __('Clubbing')}} : {{$patient->clubbing  ?? null }}</label></td>
            <td><label>{{ __('Paller')}} : {{$patient->paller  ?? null }}</label></td>
            <td><label>{{ __('Cyanosis')}} : {{$patient->cyanosis  ?? null }}</label></td>
            <td><label>{{ __('Epilepsy/Depression')}} : {{$patient->depression  ?? null }}</label></td>
        </tr>

        </tbody>
    </table>
</div>
<div class="left-side">
    <div class=" table-side">
        <table class=" table ">
            <p style="vertical-align: middle; text-align: center; border: 1px solid #ddd;background: #000; width: 200px; color: #fff; padding: 5px; font-weight: bold;" >SYSTRMIC EXAMINATION </p>
            <thead>
            <th colspan="2">Type of Examination</th>

            <th>Result</th>
            </thead>
            <tbody>
            <tr valign="center" style="border: 1px solid #ddd;" >
                <td rowspan="3" style="vertical-align: middle; border-right: 1px solid #ddd;" >Eye Vision</td>
                <td>R.Eye</td>
                <td>{{$patient->right_eye  ?? null }}</td>
            </tr>
            <tr>

                <td>L.Eye</td>
                <td>{{$patient->left_eye  ?? null }}</td>

            </tr>
            <tr>

                <td>Color Vission</td>
                <td>{{ $patient->color_vision  ?? null }}</td>

            </tr>
            <tr valign="center" >
                <td rowspan="2" style="vertical-align: middle;" >Ear</td>
                <td>R.Ear</td>
                <td> {{ $patient->right_ear  ?? null }}</td>

            </tr>
            <tr>

                <td>L.Ear</td>
                <td>{{ $patient->left_ear  ?? null }}</td>

            </tr>
            <tr>
                <td colspan="2">Cordiovascular</td>
                <td>{{ $patient->cardiovascular ?? null }}</td>
            </tr>
            <tr>
                <td colspan="2">Pulrnonary</td>
                <td>{{  $patient->pulmonary ?? null }}</td>
            </tr>
            <tr>
                <td colspan="2">Gastroenterology</td>
                <td>{{  $patient->gastroenterology ?? null }}</td>
            </tr>
            <tr>
                <td colspan="2">Neorology</td>
                <td>{{  $patient->neurology ?? null }}</td>
            </tr>
            <tr>
                <td colspan="2">Museulosheletal</td>
                <td>{{ $patient->musculoskeletal ?? null }} </td>
            </tr>
            <tr>
                <td colspan="2">Genitounnary</td>
                <td>{{ $patient->genitourinary ?? null }}</td>
            </tr>
            <tr>
                <td colspan="2">Ore-Dentel</td>
                <td>{{ $patient->oro_dental ?? null }} </td>
            </tr>
            <tr>
                <td colspan="2">Extremilies/Deformities</td>
                <td>{{ $patient->extremities ?? null }}</td>
            </tr>
            <tr>
                <td colspan="2">Varicose Veins</td>
                <td>{{ $patient->varicose_veins ?? null }}</td>
            </tr>
            <tr>
                <td colspan="2">Hernia</td>
                <td>{{ $patient->hernia ?? null }}</td>
            </tr>
            <tr>
                <td colspan="2">Hydrocele</td>
                <td>{{$patient->hydrocele ?? null }}</td>
            </tr>
           {{-- <tr>
                <td colspan="2">Hydrocele</td>
                <td>{{ $patient->hydrocele ?? null }}</td>
            </tr>--}}
            <tr>
                <td colspan="2">Radiological(Chest X-ray)</td>
                <td>{{  $patient->radiological ?? null }}</td>
            </tr>
            <tr>
                <td colspan="2">ECG</td>
                <td>{{ $patient->ecg ?? null }}</td>
            </tr>
            <tr>
                <td colspan="2">Heart</td>
                <td>{{ $patient->heart ?? null }}</td>
            </tr>
            <tr>
                <td colspan="2">Lungs</td>
                <td>{{ $patient->lungs ?? null }}</td>
            </tr>
            <tr>
                <td colspan="2">Abidamon</td>
                <td>{{ $patient->abdomen ?? null }}</td>
            </tr>
            <tr>
                <td colspan="2">Clinical Impression</td>
                <td>{{ $patient->clinical_impression ?? null }}</td>
            </tr>

            </tbody>

        </table>
    </div><!-- close table-side -->
    <p class="mention">{{$patient->name}} is {{$patient->status}} for the Mentioned job.</p>
</div>
<div class="right-side">
    <div class="table-resposive">
        <p style="vertical-align: middle; text-align: center; border: 1px solid #ddd; background: #000; width:200px; color:#fff; padding: 5px; font-weight: bold;" >LABORATORY EXAMINATION</p>
        <table class="table">

            <p style="vertical-align: middle; text-align: center; border: 1px solid #ddd; font-weight: bold;" >Serology</p>

            <thead>
            <th>Examination</th>
            <th style="text-align: center;">Result</th>
            </thead>
            <tbody>
            <tr>
                <td>Anti-HIV(1&2)</td>
                <td>{{ $patient->anti_hiv ?? null }}</td>
            </tr>
            <tr>
                <td>HBS-AQ </td>
                <td>{{ $patient->hbs_ag ?? null }} </td>
            </tr>
            <tr>
                <td>Anti-HCV  </td>
                <td>{{ $patient->anti_hcv ?? null }}</td>
            </tr>
            <tr>
                <td>VDRL  </td>
                <td>{{  $patient->vdrl ?? null }} </td>
            </tr>
            <tr>
                <td>TPHA</td>
                <td>{{ $patient->tpha ?? null }} </td>
            </tr>
            <tr>
                <td>Blood Group(ABO/Rh)</td>
                <td>{{ $patient->blood_group ?? null }}</td>
            </tr>
            <tr>
                <td colspan="2" style="text-align: center;"><strong>Urine</strong></td>
            </tr>
            <tr>
                <td>RBC</td>
                <td> {{$patient->rbc ?? null }}</td>
            </tr>
            <tr>
                <td>Pus Cells</td>
                <td>{{  $patient->pus_ceils ?? null }}</td>
            </tr>
            <tr>
                <td>Bacteria</td>
                <td> {{ $patient->bacteria ?? null }}</td>
            </tr>
            <tr>
                <td>Opiates</td>
                <td>{{  $patient->opiates ?? null }} </td>
            </tr>
            <tr>
                <td>Epithelial Cells</td>
                <td>{{  $patient->epithelial_cells ?? null }} </td>
            </tr>
            <tr>
                <td>Pregnancy Test (If Female)</td>
                <td>{{  $patient->pregnancy_test ?? null }}</td>
            </tr>

            </tbody>
        </table>
    </div><!-- table-resposive-->
    <div class="table-resposive">
        <table class="table">

            <p style="vertical-align: middle; border: 1px solid #ddd; font-weight: bold; margin-bottom: 0;  padding: 5px; background: #000; color: #fff; width: 130px;" >HEMATOLOGY</p>

            <thead>
            <th>Blood Examination</th>
            <th style="text-align: center;">Result</th>
            <th>Reference range</th>
            </thead>
            <tbody>
            <tr>
                <td>Total/WBC Count</td>
                <td>{{  $patient->total_wbc_count ?? null }}</td>
                <td>4000-11000</td>
            </tr>
            <tr>
                <td>Neutrophils</td>
                <td>{{ $patient->neutrophils ?? null }}</td>
                <td>45-74%</td>
            </tr>
            <tr>
                <td>Lymphocytes</td>
                <td>{{ $patient->lymphocytes ?? null }} </td>
                <td>25-40%</td>
            </tr>
            <tr>
                <td>Eosinophils</td>
                <td>{{  $patient->eosinophils ?? null }}</td>
                <td>1-6%</td>
            </tr>
            <tr>
                <td>Monocytes</td>
                <td>{{ $patient->monocytes ?? null }}</td>
                <td>0-8%</td>
            </tr>
            <tr>
                <td>Basophils</td>
                <td>{{ $patient->basophils ?? null }}</td>
                <td>0-3%</td>
            </tr>
            <tr>
                <td>ESR</td>
                <td>{{ $patient->esr ?? null }}</td>
                <td>M<10,F<20</td>
            </tr>
            <tr>
                <td>Hemoglobin</td>
                <td>{{ $patient->hemoglobin ?? null }}</td>
                <td>M 12-17 gm% <br> F 11-14 gm%</td>
            </tr>
            <tr>
                <td>Malaria Parasite</td>
                <td>{{ $patient->malaria_parasite ?? null }}</td>
                <td></td>
            </tr> <tr>
                <td>Micro Filaria</td>
                <td>{{ $patient->micro_filaria ?? null }}</td>
                <td></td>
            </tr>
            </tbody>
        </table>
    </div><!-- table-resposive-->

    <div class="table-resposive">
        <table class="table">

            <p style="vertical-align: middle; border: 1px solid #ddd; font-weight: bold; margin-bottom: 0;  padding: 5px; background: #000; color: #fff; width: 130px;" >BIOCHEMISTRY</p>

            <thead>
            <th>Randan Blood Sugar</th>
            <th>{{ $patient->random_blood_sugar ?? null }} </th>
            <th>80-30mg%</th>
            </thead>
            <tbody>
            <tr>
                <td>Urea</td>
                <td>{{ $patient->urea ?? null }}</td>
                <td>20-25mg%</td>
            </tr>
            <tr>
                <td> Crealnine</td>
                <td>{{ $patient->creatinine ?? null }}</td>
                <td>04-14mg%</td>
            </tr>
            <tr>
                <td> Total Direct</td>
                <td>{{  $patient->bilirubin ?? null }} </td>
                <td>0.4-1mg%</td>
            </tr>
            <tr>
                <td>SGPT</td>
                <td>{{ $patient->sgpt ?? null }} </td>
                <td>5-37 UL</td>
            </tr>
            <tr>
                <td>SGOT</td>
                <td>{{  $patient->sgot ?? null }}</td>
                <td>5-37 UL</td>
            </tr>

            <tr>
                <td colspan="3" style="text-align: center"><strong>Other</strong></td>

            </tr>
            <tr>
                <td>Cannabis</td>
                <td colspan="2">{{$patient->cannabis ?? null }}</td>
            </tr>
            <tr>
                <td>Mantoex</td>
                <td colspan="2">{{ $patient->mantoux_test ?? null }}</td>
            </tr>
            </tbody>
        </table>
    </div><!-- table-resposive-->
</div>
<div class="clear"></div>
<p class="text-contain">This Report is vaild for 2 month the date of medical Examination DR.Nabin Kumar chaudhary</p>

<a href="{{route('patients.print',$patient)}}">Print</a>
@endsection


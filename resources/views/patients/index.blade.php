@extends('layouts.app')
@section('title')
    Patients Records
@endsection
@section('content')
    <div class="row">
        <div class="col-md-12 grid-margin">
            <div class="d-flex justify-content-between flex-wrap">
                <div class="d-flex align-items-end flex-wrap">
                    <div class="mr-md-3 mr-xl-5">
                        <div class="d-flex">

                            <a class="text-muted mb-0 hover-cursor" href="{{route('home')}}">  <i class="mdi mdi-home text-muted hover-cursor"></i>Dashboard/&nbsp;</a>
                            <p class="text-primary mb-0 hover-cursor">Patient Records</p>
                        </div>
                    </div>
                </div>
            <div class="d-flex justify-content-between align-items-end flex-wrap float-right">
                <button class="btn btn-light bg-white btn-icon mr-3 d-none d-md-block" id="filter_sign" title="Filter"><i class="mdi mdi-filter"></i></button>
            </div>
        </div>
        <div class="filter-section">
            @include('patients.filter')
        </div>
            <br>
        @include('layouts.alert.alert')
        <div class="col-lg-12 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">
                    <div class="card-top">
                    <h4 class="card-title">{{ __('Patients Record') }}</h4>
                    <div class="card-description" >
                                <a href="{{route('patients.create')}}" class="btn btn-primary btn-icon-text">Create <i class="mdi mdi-plus"></i> </a>
                    </div>
                </div>
{{--                    close card-top--}}
                    <div class="table-responsive">
                        {{$dataTable->table(['class' => 'table table-hover'])}}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('style')
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/flatpickr/dist/flatpickr.min.css">
    <style>
        .card-top{
            display: flex;
            justify-content: space-between;
        }
        .card-top .card-description a {
            width: 117px;
            border-radius: 31px;
            background-color: #e73b3b;
            border: 0;
            padding: 10px;
            padding-top: 5px;
        }

         .d-flex .mdi{
             padding-right:15px;
             text-align: center;
         }
        .odd tr{
            text-align: center;
            vertical-align: middle;
        }
        .table hover th{
            white-space: nowrap;
        }
    </style>
@endsection
@section('script')
    <script src="{{ asset('/vendor/datatables/buttons.server-side.js') }}"></script>
    {{$dataTable->scripts()}}
    <script src="https://cdn.jsdelivr.net/npm/flatpickr"></script>
    <script>
        var fp = flatpickr(".date", {
            dateFormat: "Y-m-d",
        })
    </script>
@endsection

<div class="table table-head">
    @if($errors->any())
        {!! implode('', $errors->all('<p style="color:red">:message</div>')) !!}
    @endif
    <table class="table-wrap">
        <p style="vertical-align: middle; text-align: center; border: 1px solid #ddd;  width:100px;  padding: 5px; margin: auto; font-weight: bold;" >
            <select name="status">
                <option value="FIT" @if(isset($patient) && $patient->status=="FIT") selected @endif>FIT</option>
                <option value="UNFIT" @if(isset($patient) && $patient->status=="UNFIT") selected @endif>UNFIT</option>
            </select>
        </p>
        <thead>
        <tr>
            <th>EMC Code <input type="text" name="emc_code" value="{{ old('emc_code', $patient->emc_code ?? null) }}"></th>

            <th></th>
            <th>Medical Examination Date:<input type="date" name="medical_examination_date" value="{{ isset($patient) ? date("Y-m-d", strtotime($patient->medical_examination_date)):date("Y-m-d") }}{{--{{ old('medical_examination_date', isset($patient) ? $patient->medical_examination_date->format('YYYY-MM-DD ') : date('YYYY-MM-DD')) }}--}}"></th>
        </tr>
        </thead>

        <tbody>

        <tr>

            <td><input type="file" name="patient_image" id="image"></td>

            <td><input type="text" name="name"  placeholder="{{ __('Name') }}" value="{{ old('name', $patient->name ?? null) }}"></td>
            <td><input type="text" name="marital_status"  placeholder="{{ __('Marital Status') }}" value="{{ old('marital_status', $patient->marital_status ?? null) }}"></td>
            <td><input type="text" name="apply_county" placeholder="{{ __('Apply Country') }}" value="{{ old('apply_county', $patient->apply_county ?? null) }}"></td>
        </tr>
        <tr>
            <td><input type="text" name="age" value="{{ old('age', $patient->age ?? null) }}" placeholder="{{ __('Age') }}"></td>
            <td><input type="text" name="nationality" value="{{ old('nationality', $patient->nationality ?? null) }}" placeholder="{{ __('Nationlity') }}"></td>
            <td><input type="text" name="pp_issue_date" value="{{ old('pp_issue_date', $patient->pp_issue_date ?? null) }}" placeholder="{{ __('PP:Issue date') }}"></td>
            <td><input type="text" name="pp_issue_place" value="{{ old('pp_issue_place', $patient->pp_issue_place ?? null) }}" placeholder="{{ __('PP:Issue Place') }}"></td>

        </tr>
        <tr>
            <td><select name="sex">
                    <option @if(isset($patient) && $patient->sex=="Male") selected @endif>Male</option>
                    <option @if(isset($patient) && $patient->sex=="Female") selected @endif>Female</option>
                </select></td>
            <td><input type="text" name="pp_no"  value="{{ old('pp_no', $patient->pp_no ?? null) }}" placeholder="{{ __('PP NO:') }}"></td>
            <td><input type="text" name="recruiting_agency" value="{{ old('recruiting_agency', $patient->recruiting_agency ?? null) }}"  placeholder="{{ __('Recruiting Agency') }}"></td>

        </tr>
        </tbody>
    </table>
</div>
<div class="table table-head">

    <table class="table-wrap">
        Past Histry of Serious Illness and Major Surgery : <textarea name="history_illness" >{!! old('history_illness', $patient->history_illness ?? null) !!}</textarea>

        <tbody>
        <tr>
            <td><input type="text" name="height"  value="{{ old('height', $patient->height ?? null) }}" placeholder="{{ __('Height') }}"></td>
            <td><input type="text" name="pulse"  value="{{ old('pulse', $patient->pulse ?? null) }}" placeholder="{{ __('Pulse') }}"></td>
            <td><input type="text" name="jaundice"  value="{{ old('jaundice', $patient->jaundice ?? null) }}" placeholder="{{ __('Jaundice') }}"></td>
            <td><input type="text" name="ascites"  value="{{ old('ascites', $patient->ascites ?? null) }}" placeholder="{{ __('Ascites') }}"></td>
            <td><input type="text" name="lymph_node"  value="{{ old('lymph_node', $patient->lymph_node ?? null) }}" placeholder="{{ __('Lymph Node') }}"></td>
        </tr>
        <tr>
            <td><input type="text" name="weight" value="{{ old('weight', $patient->weight ?? null) }}" placeholder="{{ __('Weight') }}"></td>
            <td><input type="text" name="bp" value="{{ old('bp', $patient->bp ?? null) }}" placeholder="{{ __('BP') }}"></td>
            <td><input type="text" name="oedema" value="{{ old('oedema', $patient->oedema ?? null) }}" placeholder="{{ __('Oedema') }}"></td>
            <td><input type="text" name="allergy" value="{{ old('allergy', $patient->allergy ?? null) }}" placeholder="{{ __('Allergy') }}"></td>
            <td><input type="text" name="fungal_infection" value="{{ old('fungal_infection', $patient->fungal_infection ?? null) }}" placeholder="{{ __('Fungal infection') }}"></td>
        </tr>
        <tr>
            <td><input type="text" name="temp" value="{{ old('temp', $patient->temp ?? null) }}" placeholder="{{ __('Temperature')}}"></td>
            <td><input type="text" name="clubbing" placeholder="{{ __('Clubbing')}}" value="{{ old('clubbing', $patient->clubbing ?? null) }}"></td>
            <td><input type="text" name="paller" placeholder="{{ __('Paller')}}" value="{{ old('paller', $patient->paller ?? null) }}"></td>
            <td><input type="text" name="cyanosis" placeholder="{{ __('Cyanosis')}}" value="{{ old('cyanosis', $patient->cyanosis ?? null) }}"></td>
            <td><input type="text" name="depression" placeholder="{{ __('Epilepsy/Depression')}}" value="{{ old('depression', $patient->depression ?? null) }}"></td>
        </tr>

        </tbody>
    </table>
</div>
<div class="left-side">
    <div class=" table-side">
        <table class=" table ">
            <p style="vertical-align: middle; text-align: center; border: 1px solid #ddd;background: #000; width: 200px; color: #fff; padding: 5px; font-weight: bold;" >SYSTRMIC EXAMINATION </p>
            <thead>
            <th colspan="2">TYpe of Examination</th>

            <th>Result</th>
            </thead>
            <tbody>
            <tr valign="center" style="border: 1px solid #ddd;" >
                <td rowspan="3" style="vertical-align: middle; border-right: 1px solid #ddd;" >Eye Vision</td>
                <td>R.Eye</td>
                <td><input type="text" name="right_eye" value="{{ old('right_eye', $patient->right_eye ?? null) }}" placeholder=""></td>
            </tr>
            <tr>

                <td>L.Eye</td>
                <td><input type="text" name="left_eye" value="{{ old('left_eye', $patient->left_eye ?? null) }}" placeholder=""></td>

            </tr>
            <tr>

                <td>Color Vission</td>
                <td><input type="text" name="color_vision" value="{{ old('color_vision', $patient->color_vision ?? null) }}" placeholder=""></td>

            </tr>
            <tr valign="center" >
                <td rowspan="2" style="vertical-align: middle;" >Ear</td>
                <td>R.Ear</td>
                <td><input type="text" name="right_ear" value="{{ old('right_ear', $patient->right_ear ?? null) }}" placeholder=""></td>

            </tr>
            <tr>

                <td>L.Ear</td>
                <td><input type="text" name="left_ear" value="{{ old('left_ear', $patient->left_ear ?? null) }}" placeholder=""></td>
            </tr>
            <tr>
                <td colspan="2">Cordiovascular</td>
                <td><input type="text" name="cardiovascular" value="{{ old('cardiovascular', $patient->cardiovascular ?? null) }}" placeholder=""></td>
            </tr>
            <tr>
                <td colspan="2">Pulrnonary</td>
                <td><input type="text" name="pulmonary" value="{{ old('pulmonary', $patient->pulmonary ?? null) }}" placeholder=""></td>
            </tr>
            <tr>
                <td colspan="2">Gastroenterology</td>
                <td><input type="text" name="gastroenterology" value="{{ old('gastroenterology', $patient->gastroenterology ?? null) }}" placeholder=""></td>
            </tr>
            <tr>
                <td colspan="2">Neorology</td>
                <td><input type="text" name="neurology" value="{{ old('neurology', $patient->neurology ?? null) }}" placeholder=""></td>
            </tr>
            <tr>
                <td colspan="2">Museulosheletal</td>
                <td><input type="text" name="musculoskeletal" value="{{ old('musculoskeletal', $patient->musculoskeletal ?? null) }}" placeholder=""></td>
            </tr>
            <tr>
                <td colspan="2">Genitounnary</td>
                <td><input type="text" name="genitourinary" value="{{ old('genitourinary', $patient->genitourinary ?? null) }}" placeholder=""></td>
            </tr>
            <tr>
                <td colspan="2">Ore-Dentel</td>
                <td><input type="text" name="oro_dental" value="{{ old('oro_dental', $patient->oro_dental ?? null) }}" placeholder=""></td>
            </tr>
            <tr>
                <td colspan="2">Extremilies/Deformities</td>
                <td><input type="text" name="extremities" value="{{ old('extremities', $patient->extremities ?? null) }}" placeholder=""></td>
            </tr>
            <tr>
                <td colspan="2">Varicose Veins</td>
                <td><input type="text" name="varicose_veins" value="{{ old('varicose_veins', $patient->varicose_veins ?? null) }}" placeholder=""></td>
            </tr>
            <tr>
                <td colspan="2">Hernia</td>
                <td><input type="text" name="hernia" value="{{ old('hernia', $patient->hernia ?? null) }}" placeholder=""></td>
            </tr>
            <tr>
                <td colspan="2">Hydrocele</td>
                <td><input type="text" name="hydrocele" value="{{ old('hydrocele', $patient->hydrocele ?? null) }}" placeholder=""></td>
            </tr>
            <tr>
                <td colspan="2">Hydrocele</td>
                <td><input type="text" name="hydrocele" value="{{ old('hydrocele', $patient->hydrocele ?? null) }}" placeholder=""></td>
            </tr>
            <tr>
                <td colspan="2">Radiological(Chest X-ray)</td>
                <td><input type="text" name="radiological" value="{{ old('radiological', $patient->radiological ?? null) }}" placeholder=""></td>
            </tr>
            <tr>
                <td colspan="2">ECG</td>
                <td><input type="text" name="ecg" value="{{ old('ecg', $patient->ecg ?? null) }}" placeholder=""></td>
            </tr>
            <tr>
                <td colspan="2">Heart</td>
                <td><input type="text" name="heart" value="{{ old('heart', $patient->heart ?? null) }}" placeholder=""></td>
            </tr>
            <tr>
                <td colspan="2">Lungs</td>
                <td><input type="text" name="lungs" value="{{ old('lungs', $patient->lungs ?? null) }}" placeholder=""></td>
            </tr>
            <tr>
                <td colspan="2">Abidamon</td>
                <td><input type="text" name="abdomen" value="{{ old('abdomen', $patient->abdomen ?? null) }}" placeholder=""></td>
            </tr>
            <tr>
                <td colspan="2">Clinical Impression</td>
                <td><input type="text" name="clinical_impression" value="{{ old('clinical_impression', $patient->clinical_impression ?? null) }}" placeholder=""></td>
            </tr>

            </tbody>

        </table>
    </div><!-- close table-side -->
    <p class="mention">MR RAJU GURUNG is fit for the Mentioned job.</p>
</div>
<div class="right-side">
    <div class="table-resposive">
        <p style="vertical-align: middle; text-align: center; border: 1px solid #ddd; background: #000; width:200px; color:#fff; padding: 5px; font-weight: bold;" >LABORATORY EXAMINATION</p>
        <table class="table">

            <p style="vertical-align: middle; text-align: center; border: 1px solid #ddd; font-weight: bold;" >Serology</p>

            <thead>
            <th>Examination</th>
            <th style="text-align: center;">Result</th>
            </thead>
            <tbody>
            <tr>
                <td>Anti-HIV(1&2)</td>
                <td><input type="text" name="anti_hiv" value="{{ old('anti_hiv', $patient->anti_hiv ?? null) }}" ></td>
            </tr>
            <tr>
                <td>HBS-AQ </td>
                <td><input type="text" name="hbs_ag" value="{{ old('hbs_ag', $patient->hbs_ag ?? null) }}" ></td>
            </tr>
            <tr>
                <td>Anti-HCV  </td>
                <td><input type="text" name="anti_hcv" value="{{ old('anti_hcv', $patient->anti_hcv ?? null) }}" ></td>
            </tr>
            <tr>
                <td>VDRL  </td>
                <td><input type="text" name="vdrl" value="{{ old('vdrl', $patient->vdrl ?? null) }}" > </td>
            </tr>
            <tr>
                <td>TPHA</td>
                <td><input type="text" name="tpha" value="{{ old('tpha', $patient->tpha ?? null) }}" > </td>
            </tr>
            <tr>
                <td>Blood Group(ABO/Rh)</td>
                <td><input type="text" name="blood_group" value="{{ old('blood_group', $patient->blood_group ?? null) }}" ></td>
            </tr>
            <tr>
                <td colspan="2" style="text-align: center;"><strong>Urine</strong></td>
            </tr>
            <tr>
                <td>RBC</td>
                <td><input type="text" name="rbc" value="{{ old('rbc', $patient->rbc ?? null) }}" ></td>
            </tr>
            <tr>
                <td>Pus Cells</td>
                <td><input type="text" name="pus_ceils" value="{{ old('pus_ceils', $patient->pus_ceils ?? null) }}" ></td>
            </tr>
            <tr>
                <td>Bacteria</td>
                <td><input type="text" name="bacteria" value="{{ old('bacteria', $patient->bacteria ?? null) }}" ></td>
            </tr>
            <tr>
                <td>Opiates</td>
                <td><input type="text" name="opiates" value="{{ old('opiates', $patient->opiates ?? null) }}" ></td>
            </tr>
            <tr>
                <td>Epithelial Cells</td>
                <td><input type="text" name="epithelial_cells" value="{{ old('epithelial_cells', $patient->epithelial_cells ?? null) }}" ></td>
            </tr>
            <tr>
                <td>Pregnancy Test (If Female)</td>
                <td><input type="text" name="pregnancy_test" value="{{ old('pregnancy_test', $patient->pregnancy_test ?? null) }}" ></td>
            </tr>

            </tbody>
        </table>
    </div><!-- table-resposive-->
    <div class="table-resposive">
        <table class="table">

            <p style="vertical-align: middle; border: 1px solid #ddd; font-weight: bold; margin-bottom: 0;  padding: 5px; background: #000; color: #fff; width: 130px;" >HEMATOLOGY</p>

            <thead>
            <th>Blood Examination</th>
            <th style="text-align: center;">Result</th>
            <th>Reference range</th>
            </thead>
            <tbody>
            <tr>
                <td>Total/WBC Count</td>
                <td><input type="text" name="total_wbc_count" value="{{ old('total_wbc_count', $patient->total_wbc_count ?? null) }}"></td>
                <td>4000-11000</td>
            </tr>
            <tr>
                <td>Neutrophils</td>
                <td><input type="text" name="neutrophils" value="{{ old('neutrophils', $patient->neutrophils ?? null) }}"></td>
                <td>45-74%</td>
            </tr>
            <tr>
                <td>Lymphocytes</td>
                <td><input type="text" name="lymphocytes" value="{{ old('lymphocytes', $patient->lymphocytes ?? null) }}"></td>
                <td>25-40%</td>
            </tr>
            <tr>
                <td>Eosinophils</td>
                <td><input type="text" name="eosinophils" value="{{ old('eosinophils', $patient->eosinophils ?? null) }}"></td>
                <td>1-6%</td>
            </tr>
            <tr>
                <td>Monocytes</td>
                <td><input type="text" name="monocytes" value="{{ old('monocytes', $patient->monocytes ?? null) }}"></td>
                <td>0-8%</td>
            </tr>
            <tr>
                <td>Basophils</td>
                <td><input type="text" name="basophils" value="{{ old('basophils', $patient->basophils ?? null) }}"></td>
                <td>0-3%</td>
            </tr>
            <tr>
                <td>ESR</td>
                <td><input type="text" name="esr" value="{{ old('esr', $patient->esr ?? null) }}"></td>
                <td>M<10,F<20</td>
            </tr>
            <tr>
                <td>Hemoglobin</td>
                <td><input type="text" name="hemoglobin" value="{{ old('hemoglobin', $patient->hemoglobin ?? null) }}"></td>
                <td>M 12-17 gm% <br> F 11-14 gm%</td>
            </tr>
            <tr>
                <td>Malaria Parasite</td>
                <td><input type="text" name="malaria_parasite" value="{{ old('malaria_parasite', $patient->malaria_parasite ?? null) }}"></td>
                <td></td>
            </tr> <tr>
                <td>Micro Filaria</td>
                <td><input type="text" name="micro_filaria" value="{{ old('micro_filaria', $patient->micro_filaria ?? null) }}"></td>
                <td></td>
            </tr>
            </tbody>
        </table>
    </div><!-- table-resposive-->

    <div class="table-resposive">
        <table class="table">

            <p style="vertical-align: middle; border: 1px solid #ddd; font-weight: bold; margin-bottom: 0;  padding: 5px; background: #000; color: #fff; width: 130px;" >BIOCHEMISTRY</p>

            <thead>
            <th>Randan Blood Sugar</th>
            <th><input type="text" name="random_blood_sugar" value="{{ old('random_blood_sugar', $patient->random_blood_sugar ?? null) }}"></th>
            <th>80-30mg%</th>
            </thead>
            <tbody>
            <tr>
                <td>Urea</td>
                <td><input type="text" name="urea" value="{{ old('urea', $patient->urea ?? null) }}"></td>
                <td>20-25mg%</td>
            </tr>
            <tr>
                <td> Crealnine</td>
                <td><input type="text" name="creatinine" value="{{ old('creatinine', $patient->creatinine ?? null) }}"></td>
                <td>04-14mg%</td>
            </tr>
            <tr>
                <td> Total Direct</td>
                <td><input type="text" name="bilirubin" value="{{ old('bilirubin', $patient->bilirubin ?? null) }}"></td>
                <td>0.4-1mg%</td>
            </tr>
            <tr>
                <td>SGPT</td>
                <td><input type="text" name="sgpt" value="{{ old('sgpt', $patient->sgpt ?? null) }}"></td>
                <td>5-37 UL</td>
            </tr>
            <tr>
                <td>SGOT</td>
                <td><input type="text" name="sgot" value="{{ old('sgot', $patient->sgot ?? null) }}"></td>
                <td>5-37 UL</td>
            </tr>

            <tr>
                <td colspan="3" style="text-align: center">Other</td>

            </tr>
            <tr>
                <td>Cannabis</td>
                <td colspan="2"><input type="text" name="cannabis" value="{{ old('cannabis', $patient->cannabis ?? null) }}"></td>
            </tr>
            <tr>
                <td>Mantoex</td>
                <td colspan="2"><input type="text" name="mantoux_test" value="{{ old('mantoux_test', $patient->mantoux_test ?? null) }}"></td>
            </tr>
            </tbody>
        </table>
    </div><!-- table-resposive-->
</div>
<div class="clear"></div>
<p class="text-contain">This Report is vaild for 2 month the date of medical Examination DR.Nabin Kumar chaudhary</p>
<button type="submit">@if(isset($patient)) Update @else Save @endif</button>

@extends('layouts.app')
@section('title')
    Profile
@endsection
@section('content')
    @include('layouts.alert.alert')
    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div><br />
    @endif
    <div class="row">

        <div class="col-md-8 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">Company Information</h4>
                    <div class="card-description">
                        <img src="" id="company_logo" style="width: 75px; border-radius: 30px; ">
                        <img src="{{asset('admin/images/logo/'.$information->logo)}}" id="old_company_logo" style="width: 75px; border-radius: 30px; ">
                    </div>

                    <form class="forms-sample" method="POST" action="{{route('information.update')}}" enctype="multipart/form-data">

                        @csrf
                        @method('PATCH')

                        <div class="form-group">
                            <label for="exampleInputUsername1">{{ __('Company Name') }}</label>
                            <input type="text" class="form-control" name="company_name" id="company_name"
                                   value="{{ old('company_name',$information->company_name) }}" placeholder="{{ __('Company Name') }}">
                        </div>
                        <div class="form-group">
                            <label>{{ __('Company Logo') }}</label>
                            <input type="file" name="company_logo"  id="logo" class="file-upload-default">
                            <div class="input-group col-xs-12">
                                <input type="text" class="form-control file-upload-info" disabled placeholder="Upload Image">
                                <span class="input-group-append">
                          <button class="file-upload-browse btn btn-primary" type="button">Upload</button>
                        </span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputUsername1">{{ __('Title') }}</label>
                            <input type="text" class="form-control" name="title" id="title"
                                   value="{{ old('title',$information->title) }}" placeholder="{{ __('Title') }}">
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail1">{{ __('Email address') }}</label>
                            <input type="email" class="form-control" id="email" name="email" value="{{ old('email',$information->email) }}" placeholder="{{ __('Email address') }}">
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail1">{{ __('Website URL') }}</label>
                            <input type="text" class="form-control" id="website" name="website" value="{{ old('website',$information->website) }}" placeholder="{{ __('Website URL') }}">
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="exampleInputAddress1">{{ __('Address') }}</label>
                                    <input type="text" class="form-control" id="address" name="address" value="{{ old('address',$information->address) }}" placeholder="{{ __('Address') }}">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="exampleInputEmail1">{{ __('City') }}</label>
                                    <input type="text" class="form-control" id="city" name="city" value="{{ old('city',$information->city) }}" placeholder="{{ __('City') }}">
                                </div>
                            </div>

                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="exampleInputEmail1">{{ __('Phone') }}</label>
                                    <input type="text" class="form-control" id="phone" name="phone" value="{{ old('phone',$information->phone) }}" placeholder="{{ __('Phone') }}">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="exampleInputPhone">{{ __('Landline') }}</label>
                                    <input type="text" class="form-control" name="landline" id="landline" value="{{ old('landline',$information->landline) }}" placeholder="{{ __('Landline') }}" >
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="exampleInputPhone">{{ __('Other Phone/Landline') }}</label>
                                    <input type="text" class="form-control" name="landline1" id="landline1" value="{{ old('landline1',$information->landline1) }}" placeholder="{{ __('Other Phone/Landline') }}" >
                                </div>
                            </div>
                        </div>



                        <div class="form-group">
                            <label for="exampleInputPhone">{{ __('Affiliated to') }}</label>
                            <input type="text" class="form-control" name="affiliated" id="affiliated" value="{{ old('affiliated',$information->affiliated) }}" placeholder="{{ __('Affiliated to') }}" >
                        </div>
                        <div class="form-group">
                            <label for="exampleInputPhone">{{ __('Government Registration Number') }}</label>
                            <input type="text" class="form-control" name="gov_regno" id="gov_regno" value="{{ old('gov_regno',$information->gov_regno) }}" placeholder="{{ __('Government Registration Number') }}" >
                        </div>
                      {{--  <div class="form-group">

                            <label for="exampleInputImage">{{__('Image') }}</label>
                            <input type="file" class="form-control" name="user_image" id="image">
                            <img id="user_image" src="" alt="user image" width="100px;" style="display: none"/>
                            <img id="old_user_image" src="{{asset('admin/images/user/'.auth()->user()->image)}}" alt="user image" width="100px;" />
                        </div>--}}
                        <div class='dropzone'></div>
                        <button type="submit" class="btn btn-primary btn-rounded mr-2">{{ __('Update Info') }}</button>

                    </form>
                </div>
            </div>
        </div>

    </div>
    @endsection
@section('script')
    <script src="{{asset('admin/js/file-upload.js')}}"></script>
    <script>

        function readURL(input) {


            if (input.files && input.files[0]) {

                var reader = new FileReader();
                reader.onload = function(e) {
                    $('#company_logo').show();
                    $('#old_company_logo').hide();

                    $('#company_logo').attr('src', e.target.result).slideDown();;
                };

                reader.readAsDataURL(input.files[0]);
            }
        }

        $("#logo").change(function() {
            readURL(this);
        });
    </script>
    @endsection

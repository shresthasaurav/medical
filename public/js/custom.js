
    $('#old_password').keyup(function (target) {
        let old_password=$('#old_password').val();
        var ajax = $('#old_password').attr('data-ajax');
        var data = {
            '_token' : $('meta[name=csrf-token]').attr('content'),
            old_password: old_password,

        };
        $.ajax({
            type:'post',
            url: ajax,
            data:data,
            success:function (response) {
                if(response=='false'){
                    $('#message').html("<p style='color: red'>Incorrect Password</p>");

                }else if(response=='true'){
                    $('#message').html("<p style='color: green'>Correct Password</p>").hide(selectedEffect, options, 500);

                }
            }
        })
    });



    $("#filter_sign").click(function() {
        $(".filter-section").toggle();
    });


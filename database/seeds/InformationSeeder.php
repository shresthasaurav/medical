<?php

use Illuminate\Database\Seeder;

class InformationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('information')->insert([
            'company_name' => 'EON MEDICAL CENTER PVT.LTD',
            'title' => 'Medical Report Form',
            'address' => 'LAZIMPAT',
            'city' => 'KATHMANDU',
            'phone' => '+977-1-4442440',
            'landline' => '4442441',
            'landline1' => '4442450',
            'website' => 'www.eonmedicalcenter.com',
            'email' => 'info@eonmedicalcenter.com',
            'affiliated' => 'Ministry of Labour and Employment',
            'gov_regno' => '60496-065/066',
            'created_at' => Carbon::now()->subDays(14),
            'updated_at' => Carbon::now()->subDays(14)
        ]);
    }
}

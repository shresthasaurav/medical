<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePatientsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('patients', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('emc_code')->nullable();
            $table->bigInteger('age')->nullable();
            $table->string('sex')->nullable();
            $table->string('marital_status')->nullable();
            $table->string('nationality')->nullable();
            $table->string('pp_no')->nullable();
            $table->string('apply_county')->nullable();
            $table->string('pp_issue_date')->nullable();
            $table->string('pp_issue_place')->nullable();
            $table->string('recruiting_agency')->nullable();

            $table->string('image')->nullable();
            $table->dateTime('medical_examination_date');
            $table->string('status');

            $table->string('history_illness')->nullable();
            $table->string('height')->nullable();
            $table->string('weight')->nullable();
            $table->string('temp')->nullable();
            $table->string('pulse')->nullable();
            $table->string('bp')->nullable();
            $table->string('clubbing')->nullable();

            $table->string('jaundice')->nullable();
            $table->string('oedema')->nullable();
            $table->string('paller')->nullable();
            $table->string('ascites')->nullable();
            $table->string('allergy')->nullable();
            $table->string('cyanosis')->nullable();
            $table->string('lymph_node')->nullable();
            $table->string('fungal_infection')->nullable();
            $table->string('depression')->nullable();

            $table->string('right_eye')->nullable();
            $table->string('left_eye')->nullable();
            $table->string('color_vision')->nullable();
            $table->string('right_ear')->nullable();
            $table->string('left_ear')->nullable();
            $table->string('cardiovascular')->nullable();
            $table->string('pulmonary')->nullable();
            $table->string('gastroenterology')->nullable();
            $table->string('neurology')->nullable();
            $table->string('musculoskeletal')->nullable();
            $table->string('genitourinary')->nullable();
            $table->string('oro_dental')->nullable();
            $table->string('extremities')->nullable();
            $table->string('varicose_veins')->nullable();
            $table->string('hernia')->nullable();
            $table->string('hydrocele')->nullable();
            $table->string('radiological')->nullable();
            $table->string('ecg')->nullable();
            $table->string('heart')->nullable();
            $table->string('lungs')->nullable();
            $table->string('abdomen')->nullable();
            $table->string('clinical_impression')->nullable();

            $table->string('anti_hiv')->nullable();
            $table->string('hbs_ag')->nullable();
            $table->string('anti_hcv')->nullable();
            $table->string('vdrl')->nullable();
            $table->string('tpha')->nullable();
            $table->string('blood_group')->nullable();
            $table->string('rbc')->nullable();
            $table->string('pus_ceils')->nullable();
            $table->string('bacteria')->nullable();
            $table->string('opiates')->nullable();
            $table->string('epithelial_cells')->nullable();
            $table->string('pregnancy_test')->nullable();

            $table->text('total_wbc_count')->nullable();
            $table->text('neutrophils')->nullable();
            $table->text('lymphocytes')->nullable();
            $table->text('eosinophils')->nullable();
            $table->text('monocytes')->nullable();
            $table->text('basophils')->nullable();
            $table->text('esr')->nullable();
            $table->text('hemoglobin')->nullable();
            $table->text('malaria_parasite')->nullable();
            $table->text('micro_filaria')->nullable();

            $table->text('random_blood_sugar')->nullable();
            $table->text('urea')->nullable();
            $table->text('creatinine')->nullable();
            $table->text('bilirubin')->nullable();
            $table->text('sgpt')->nullable();
            $table->text('sgot')->nullable();
            $table->text('others')->nullable();
            $table->text('cannabis')->nullable();
            $table->text('mantoux_test')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('patients');
    }
}

<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Route::get('/', function () {
    return view('welcome');
});*/
Route::get('/', 'HomeController@index')->name('home');
/**/
Route::group(['middleware' => 'auth'], function () {
    Route::get('/dashboard','HomeController@index')->name('home');
    //Information
    Route::get('information', ['as' => 'information.edit', 'uses' => 'InformationController@edit']);
    Route::patch('information', ['as' => 'information.update', 'uses' => 'InformationController@update']);
    //Profile
    Route::get('profile', ['as' => 'profile.edit', 'uses' => 'ProfileController@edit']);
    Route::patch('profile', ['as' => 'profile.update', 'uses' => 'ProfileController@update']);
    Route::post('check-old-pwd','ProfileController@checkOldPassword')->name('checkOldPassword');
    Route::post('update-old-pwd','ProfileController@updateOldPassword')->name('updateOldPassword');
    //Patient
    Route::resource('patients','PatientController');
    Route::get('print/patients/{patient}', ['as' => 'patients.print', 'uses' => 'PatientController@printPatient']);
    Route::get('patients/export/{patient}', 'PatientController@export')->name('patientExport');

});


Auth::routes([
    'register' => false, // Registration Routes...
]);


Route::get('/home', 'HomeController@index')->name('home');

<?php

namespace App\Http\Controllers;

use App\Information;
use Illuminate\Http\Request;
use Image;
use File;

class InformationController extends Controller
{

    public function show(Information $information)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Information  $information
     * @return \Illuminate\Http\Response
     */
    public function edit(Information $information)
    {
        $information=$information::first();
        return view('information.edit',compact('information'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Information  $information
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Information $information)
    {
        $information=$information->first();

        $rules=[
            'company_name' => 'required',
            'title' => 'required',
            'email' => 'required|email',
            'website' => 'required',
            'city' => 'required',
            'address' => 'required',
            'phone' => 'required',
            'landline' => 'required',
            'affiliated' => 'required',
            'gov_regno' => 'required',
        ];
        $customMessages = [
            'company_name.required'  => 'Company Name Required',
            'title.required'    => 'This field should not be empty',
            'email.required'    => 'This Email should not be empty',
            'website.required'    => 'This Website should not be empty',
            'city.required'    => 'This City should not be empty',
            'address.required'    => 'This Address should not be empty',
            'phone.required'    => 'This Contact should not be empty',
            'landline.required'    => 'This Landline should not be empty',
            'affiliated.required'    => 'Affilidated to what is neccessary',
            'gov_regno.required'    => 'Government Registration Number  is neccessary',
        ];

        $information->fill($request->merge([
            $this->validate($request, $rules, $customMessages)
        ])->all());

        if($request->hasFile('company_logo')){

            $image_tmp=$request->file('company_logo');

            $image_path = 'admin/images/logo/'. $information->logo;

            if(File::exists($image_path)){
                File::delete($image_path);
            }
            if($image_tmp->isValid()){
                $image_name=$image_tmp->getClientOriginalName();

                $extension=$image_tmp->getClientOriginalExtension();

                $imageName=$image_name.'-'.rand(111,99999).'.'.$extension;

                $image_path='admin/images/logo/'.$imageName;
                Image::make($image_tmp)->save($image_path);
                $information->logo=$imageName;
            }
        }
        $information->update();
        return  redirect()->back()->with('flash_success_message','Information has been Updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Information  $information
     * @return \Illuminate\Http\Response
     */
    public function destroy(Information $information)
    {
        //
    }
}

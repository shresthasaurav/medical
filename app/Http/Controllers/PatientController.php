<?php

namespace App\Http\Controllers;

use App\DataTables\PatientsDataTable;
use App\Exports\PatientExport;
use App\Http\Requests\PatientRequest;
use App\Patient;
use Illuminate\Http\Request;
use Image;
use File;
use Maatwebsite\Excel\Facades\Excel;

class PatientController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request,PatientsDataTable $dataTable)
    {
        $activefilters = $request->all();

        return $dataTable->render('patients.index',compact('activefilters'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('patients.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Patient $patient,PatientRequest $request)
    {
        $patient->fill($request->all());
        if($request->hasFile('patient_image')){
            $image_tmp=$request->file('patient_image');

            if($image_tmp->isValid()){
                $image_name=$image_tmp->getClientOriginalName();

                $extension=$image_tmp->getClientOriginalExtension();

                $imageName=$image_name.'-'.rand(111,99999).'.'.$extension;

                $image_path='admin/images/patients/'.$imageName;
                Image::make($image_tmp)->save($image_path);
            }
        }
        $patient->image=$imageName;

        $patient->save();

         return redirect()->route('patients.index')->with('flash_success_message','New Patient Record has been Created');;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Patient  $patient
     * @return \Illuminate\Http\Response
     */
    public function show(Patient $patient)
    {
        return view('patients.show',compact('patient'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Patient  $patient
     * @return \Illuminate\Http\Response
     */
    public function edit(Patient $patient)
    {
        return view('patients.edit',compact('patient'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Patient  $patient
     * @return \Illuminate\Http\Response
     */
    public function update(PatientRequest $request, Patient $patient)
    {

        $patient->fill($request->all());
        if($request->hasFile('patient_image')){
            $image_tmp=$request->file('patient_image');

            $image_path = $patient->image();

            if(File::exists($image_path)){
                File::delete($image_path);
            }

            if($image_tmp->isValid()){
                $image_name=$image_tmp->getClientOriginalName();

                $extension=$image_tmp->getClientOriginalExtension();

                $imageName=$image_name.'-'.rand(111,99999).'.'.$extension;

                $image_path='admin/images/patients/'.$imageName;

                Image::make($image_tmp)->save($image_path);
                $patient->image=$imageName;
            }
        }
        $patient->update();
        return redirect()->route('patients.index')->with('flash_success_message','Patient Record has been Updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Patient  $patient
     * @return \Illuminate\Http\Response
     */
    public function destroy(Patient $patient)
    {
        if(!empty($patient->image))
            $image_path = $patient->image();
        if(File::exists($image_path)){
            File::delete($image_path);
        }

         $patient->delete();

        return redirect()->route('patients.index')->with('flash_success_message','Patient Record has been Deleted');

    }
    public function printPatient(Patient $patient){

        return view('patients.print',compact('patient'));
    }

    public function export(Patient $patient)
    {
        $patient=$patient::where('id',$patient->id)->get();

        return Excel::download(new PatientExport($patient), 'patient.xlsx');
    }
}

<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Image;
use Illuminate\Support\Facades\Hash;
use File;
class ProfileController extends Controller
{

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit()
    {

        return view('profile.edit');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {

        auth()->user()->fill($request->all());
        if($request->hasFile('user_image')){

            $image_tmp=$request->file('user_image');

            $image_path = 'admin/images/user/'.auth()->user()->image;

            if(File::exists($image_path)){
                File::delete($image_path);
            }
            if($image_tmp->isValid()){
                $image_name=$image_tmp->getClientOriginalName();

                $extension=$image_tmp->getClientOriginalExtension();

                $imageName=$image_name.'-'.rand(111,99999).'.'.$extension;

                $image_path='admin/images/user/'.$imageName;
                Image::make($image_tmp)->save($image_path);
                auth()->user()->image=$imageName;
            }
        }


        auth()->user()->update();
        return  redirect()->back()->with('flash_success_message','Profile has been Updated');
    }

    public function checkOldPassword(Request $request){
       if(Hash::check($request->old_password,auth()->user()->password)){
           echo "true";
       }else{
           echo "false";
       }
    }
    public function updateOldPassword(Request $request){
        if($request->isMethod('post')){
            if(Hash::check($request->old_password,auth()->user()->password)){
                if($request->new_password==$request->password_confirmation){
                        User::where('id',auth()->user()->id)->update(['password' => Hash::make($request->new_password)]);
                     return redirect()->back()->with('flash_success_message','New Password has been Changed');
                }else{
                    return redirect()->back()->with('flash_error_message','Confirmation password doesnot match with your new Password');
                }
            }else{
                return redirect()->back()->with('flash_error_message','Your Current Password doesnot match ! Please Try Again');
            }
        }
    }
}

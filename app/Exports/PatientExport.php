<?php

namespace App\Exports;

use App\Patient;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;

class PatientExport implements FromCollection, WithHeadings
{
    /**
    * @return \Illuminate\Support\Collection
    */

    public function __construct($patient)
    {
        $this->patient = $patient;

    }

    public function collection()
    {
       return $this->patient;
    }
    public function headings(): array
    {
        return ["ID", "Name", "EMC CODE","AGE","Martial Status","Nationality","PP No","Apply Country"
            ,"PP Issue Date","PP Issue Place","Recruiting Agency","Medical Examination Date","Status","History Illness","Height","Weight","Temperature","Pulse","BP","Clubbing","Jaundice","Oedema"
            ,"paller","ascites","allergy","cyanosis","lymph_node","fungal_infection","depression"
            ,"right_eye","left_eye","color_vision","right_ear","left_ear","cardiovascular","pulmonary","gastroenterology","neurology","musculoskeletal","genitourinary"
            ,"oro_dental","extremities","varicose_veins","hernia","hydrocele","radiological","ecg","heart","lungs","Abdomen","Clinical Impression"
            ,"Anti hiv", "HBS Ag", "Anti hcv", "VDRL", "TPHA", "Blood Group", "RBC", "Pus Ceils", "Bacteria", "Opiates", "Epithelial Cells", "Pregnancy Test"
            ,"Total WBC Wount", "Neutrophils", "Lymphocytes", "Eosinophils", "Monocytes", "Basophils", "ESR", "Hemoglobin", "Malaria Parasite", "Micro Filaria"
            ,"Random Blood Sugar","Urea","Creatinine","Bilirubin","SGPT","SGOT","Others","Cannabis","Mantoux Test"
        ];
    }
}

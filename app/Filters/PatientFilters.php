<?php
namespace App\Filters;
use Illuminate\Http\Request;
use Carbon\Carbon;

class PatientFilters extends QueryFilters
{
    protected $request;

    public function __construct(Request $request)
    {
        $this->request = $request;

        parent::__construct($request);
    }
    public function min_age($term = null) {
        if ($term) {
            return $this->builder->where('age', '>=', "$term");
        }
    }
    public function max_age($term = null) {
        if ($term) {
            return $this->builder->where('age', '<=', "$term");
        }
    }

    public function from($term = null) {
        if ($term) {
            return $this->builder->where('medical_examination_date', '>=', Carbon::parse($term));
        }
    }
    public function to($term = null) {
        if ($term) {
            return $this->builder->where('medical_examination_date', '<=', Carbon::parse($term));
        }
    }

    public function gender($term = null) {
        if ($term) {
            return $this->builder->where('sex', '=', "$term");
        }
    }
    public function status($term = null) {
        if ($term) {
            return $this->builder->where('status', '=', "$term");
        }
    }
}

?>

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Traits\Filterable;

class Patient extends Model
{
    use Filterable;
    protected $fillable = [
        'name','name','age','sex','marital_status','nationality','pp_no','apply_county','pp_issue_date', 'pp_issue_place', 'recruiting_agency'
        ,'history_illness','height','weight','temp', 'pulse', 'bp', 'clubbing'
        ,'jaundice','oedema','paller','ascites','allergy','cyanosis','lymph_node','fungal_infection','depression'
        ,'right_eye','left_eye','color_vision','right_ear','left_ear','cardiovascular','pulmonary','gastroenterology','neurology','musculoskeletal','genitourinary'
        ,'oro_dental','extremities','varicose_veins','hernia','hydrocele','radiological','ecg','heart','lungs','abdomen','clinical_impression'
        ,'anti_hiv', 'hbs_ag', 'anti_hcv', 'vdrl', 'tpha', 'blood_group', 'rbc', 'pus_ceils', 'bacteria', 'opiates', 'epithelial_cells', 'pregnancy_test'
        ,'total_wbc_count', 'neutrophils', 'lymphocytes', 'eosinophils', 'monocytes', 'basophils', 'esr', 'hemoglobin', 'malaria_parasite', 'micro_filaria'
        ,'random_blood_sugar','urea','creatinine','bilirubin','sgpt','sgot','others','cannabis','mantoux_test'
        ,'image','medical_examination_date','status','emc_code'
    ];
    protected $dates = ['medical_examination_date'];
   /* protected $casts = [''];*/

    public function image(){

        return 'admin/images/patients/'.$this->image;
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Information extends Model
{
    protected $fillable = [
        'company_name', 'title', 'logo', 'address', 'city', 'phone', 'landline',
        'landline1', 'website', 'email', 'affiliated', 'gov_regno',
    ];
}

<?php

namespace App\DataTables;

use App\Filters\PatientFilters;
use App\Patient;
use Yajra\DataTables\DataTables;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Html\Editor\Editor;
use Yajra\DataTables\Html\Editor\Fields;
use Yajra\DataTables\Services\DataTable;


class PatientsDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable(Patient $patient,PatientFilters $filters)
    {

        $patients=Patient::filter($filters)->get();
        return DataTables::of($patients)
            ->addIndexColumn()
            ->addColumn('name', function($patient){
                return $patient->name;
            })
            ->addColumn('emc_code', function($patient){
                return $patient->emc_code;
            })
            ->addColumn('status', function($patient){
                if($patient->status=="FIT"){
                    return '<label class="badge badge-success">FIT</label>';
                }else{
                    return '<label class="badge badge-danger">UNFIT</label>';
                }

            })

            ->addColumn('image', function($patient){
                if(!empty($patient->image)){
                    return '<img src=" '.asset($patient->image()).' " style="width: 69px;height: 71px;
                    border-radius: 16%!important; ""/>';
                }else{
                    return "No Image";
                }

            })

            ->addColumn('age', function($patient){
                return $patient->age;
            })

            ->addColumn('action', function($patient){
                return view('patients.action', compact('patient'));
            })
            ->addColumn('date', function($patient){
                return date('Y-M-d', strtotime($patient->medical_examination_date));
            })

        ->rawColumns(['image','status','action']);
    }


    /**
     * Get query source of dataTable.
     *
     * @param \App\App\Patient $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(Patient $model)
    {
        return $model->newQuery();
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->setTableId('ajax-datatable')
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->dom('Bfrtip')
            ->orderBy(1)
            ->autoWidth(false)
            ->buttons(
                Button::make('excel'),
                Button::make('print'),
                Button::make('pageLength'),
                Button::make('reset')
            )

            ->lengthMenu([25,50,100]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            Column::computed('DT_RowIndex')->title(__('S.N')),
            Column::computed('image')->title(__('Image')
            ),
            Column::make('emc_code')->title(__('EMC Code'))
                ->searchable(true)
                ->orderable(true),
            Column::make('name')->title(__('Name'))
                ->searchable(true)
                ->orderable(true),
            Column::computed('date')->title(__('Medical Examination <br> Date'))
                ->searchable(true)
                ->orderable(false),
            Column::make('age')->title(__('Age')),
            Column::computed('status')->title(__('Status')),

            Column::computed('action')->title('Action')
                ->printable(false)
                ->exportable(false)
                ->addClass('text-center'),
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'Patients_' . date('YmdHis');
    }
}
